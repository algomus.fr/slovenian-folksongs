import pandas as pd
import json
import os
import re

def contour_conversion(contour):
    if contour == 'CVX':
        contour='↗↘'
    elif contour == 'CCV':
        contour='↘↗'
    elif contour == 'DESC':
        contour='↘'
    elif contour == 'ASC':
        contour='↗'
    elif contour == 'H':
        contour='→'
    elif contour == 'HD':
        contour='→↘'
    elif contour == 'HA':
        contour='→↗'
    elif contour == 'AH':
        contour='↗→'
    elif contour == 'DH':
        contour='↘→'
    else:
        contour='?'
    
    return contour


# EXCEL:
df = pd.read_excel("slp-data/slp_phrases.xlsx")

# Variables
type_variable = "Type"
variant_variable = "Variant"
additional_mark_variable = "Additional_mark"
pl_variable = "Label"
ct_variable = "Contours"
#hs_variable = "PH_Start"
#he_variable = "PH_End"
measure_phrase_variable = "Measure_Phrase"
lyric_structure = "Lyric"
verse = "Verse_GNI"
verse_23 = "Verse_GNI23"
measure_nr_variable = "Measure_Nr"
ph_pos = "Phrase_position"

# Tune ID
current_type = None
current_variant = None
current_additional_mark = None
labels = []

# Layout
layout = [
    {"filter": {"type": "PhraseLabel"}, "style": {"line": "top.1", "color": "#0000ff"}},
    {"filter": {"type": "Contour"}, "style": {"line": "top.2", "color": "#ff3399"}},
    {"filter": {"type": "LyricSyllables"}, "style": {"line": "bot.2", "color": "#ff0000"}},
    {"filter": {"type": "Verse"}, "style": {"line": "bot.1", "color": "#00ff00"}},
    {"filter": {"type": "VerseGNI2023"}, "style": {"line": "bot.3", "color": "#99ff99"}}
]

for index, row in df.iterrows():
    element_type = str(row[type_variable])
    element_variant = str(row[variant_variable])
    element_additional_mark = str(row[additional_mark_variable])
    pl_tag = row[pl_variable]
    ct_tag = row[ct_variable]
    ct_tag = contour_conversion(ct_tag)
    lyric_tag = row[lyric_structure]
    verse_tag = row[verse]
    verse23_tag = row[verse_23]
    phrase_pos =row[ph_pos]
    measure_nr=row[measure_nr_variable]
    measure_phrase_value = str(row[measure_phrase_variable])

    # match by ID
    if (
        current_type != element_type
        or current_variant != element_variant
        or current_additional_mark != element_additional_mark
    ):
        # Save the previous ID's labels to a JSON file
        if current_type is not None and labels:
            # JSON + universal label meta
            json_data = {
                "labels": labels,
                "meta": {
                    "layout": layout,
                },
            }

            # json to string
            json_string = json.dumps(json_data, indent=2)

            # json to file
            filename = "slp-annotations/" + f"{current_type}-{current_variant}--{current_additional_mark}.dez"
            with open(filename, "w") as file:
                file.write(json_string)

        # Reset ID
        current_type = element_type
        current_variant = element_variant
        current_additional_mark = element_additional_mark
        labels = []

    if (
        index + 1 < len(df)
        and (
            current_type == str(df.at[index + 1, type_variable])
            and current_variant == str(df.at[index + 1, variant_variable])
            and current_additional_mark == str(df.at[index + 1, additional_mark_variable])
        )
    ):
        end_measure_phrase_value = str(df.at[index + 1, measure_phrase_variable])
        
    if phrase_pos == 'L':
        end_measure_phrase_value=str(measure_nr+1)
        
        # if '+' in end_measure_phrase_value:
        #     new_number=end_measure_phrase_value.split('+')
        #     new_last_measure=int(new_number[0])+1
        #     end_measure_phrase_value=str(new_last_measure)+'+'+new_number[-1]

        # elif '-' in end_measure_phrase_value:
        #     new_number=end_measure_phrase_value.split('-')
        #     new_last_measure=int(new_number[0])+1
        #     end_measure_phrase_value=str(new_last_measure)
        # elif 'nan' in end_measure_phrase_value:
        #     end_measure_phrase_value="0"
        # else:
        #     end_measure_phrase_value=str(int(end_measure_phrase_value)+1)

    else:
        end_measure_phrase_value = str(end_measure_phrase_value)

    # New dict: Contour, PhraseLabel, Verse, VerseGNI
    label1 = {
        "type": "Contour",
        "tag": ct_tag,
        "start": "m" + measure_phrase_value,
        "end": "m" + end_measure_phrase_value,
    }
    label2 = {
        "type": "PhraseLabel",
        "tag": pl_tag,
        "start": "m" + measure_phrase_value,
        "end": "m" + end_measure_phrase_value,
    }
    label3 = {
        "type": "LyricSyllables",
        "tag": lyric_tag,
        "start": "m" + measure_phrase_value,
        "end": "m" + end_measure_phrase_value,
    }
    label4 = {
        "type": "Verse",
        "tag": verse_tag,
        "start": "m" + measure_phrase_value,
        "end": "m" + end_measure_phrase_value,
    }
    label5 = {
        "type": "VerseGNI2023",
        "tag": verse23_tag,
        "start": "m" + measure_phrase_value,
        "end": "m" + end_measure_phrase_value,
    }

    # Check and exclude elements with specified conditions
    if pd.notna(label2["tag"]):
        labels.append(label2)
    if pd.notna(label1["tag"]):
        labels.append(label1)
    if pd.notna(label3["tag"]):
        labels.append(label3)
    if pd.notna(label4["tag"]):
        labels.append(label4)
    if pd.notna(label5["tag"]):
        labels.append(label5)

# Final step
if current_type is not None and labels:
    # JSON + Meta
    json_data = {
        "labels": labels,
        "meta": {
            "layout": layout,
            "creationButtons": [{"type": "PhraseLabel", "tag": "A"}],
        },
    }

    # json to str
    json_string = json.dumps(json_data, indent=2)

    # json to file
    filename = "slp-annotations/" + f"{current_type}-{current_variant}--{current_additional_mark}.dez"
    with open(filename, "w") as file:
        file.write(json_string)

# REMOVE --NAN from filenames:
folder_path = "slp-annotations/"

for filename in os.listdir(folder_path):
    file_path = os.path.join(folder_path, filename)
    if "--nan" in filename:
        new_filename = filename.replace("--nan", "")
        new_file_path = os.path.join(folder_path, new_filename)
        os.rename(file_path, new_file_path)
