# Slovenian Folk Song Ballads: Family Ballads

## Description

The collection of Slovenian folk song ballads contains transcribed field material collected by Slovenian ethnologists, folklorists, ethnomusicologists, and various colleagues of [GNI ZRC SAZU](https://gni.zrc-sazu.si/en) spanning the years 1819 to 1995.

Categorised thematically as family ballads, this collection features 402 folk songs, and includes the initial verse of the lyrics, extensive metadata, and musical analysis, encompassing contours, harmony, and song structure (melody and lyric). 23 of these songs have historical recordings. The full corpus with visualised scores and annotations is available on [Dezrann](https://dezrann.net/explore/slovenian-folk-songs).

## References

1. Vanessa Nina Borsan, Mathieu Giraud, Richard Groult, and Thierry Lecroq. [Adding context to content improves pattern matching: A study on Slovenian folksongs](https://hal.science/hal-04142315v1/file/2023-ismir-descriptors-pattern-matching.pdf). In International Society for Music Information Retrieval Conference (ISMIR 2023), 2023.

2. Borsan, Vanessa Nina et al. "The Digitised Dataset of Slovenian Folk Songs Ballads"
[Under submission]

## License, Availability

The dataset (scores in `.musicXML`, recordings in `.mp3`, metadata, and annotations in `.csv`/`.json` files) is released under the open CC BY-NC-SA 4.0 license and available for download at http://algomus.fr/data. We also release the songs on the [Dezrann](http://www.dezrann.net/explore/slovenian-folk-songs) open-source platform, where all annotations for each song are visualised, and, when audio files are available, synchronised to the scores.

## Annotations

### Music descriptors

Music descriptors can pertain to either the content of the music or represent the music content itself. Below, we provide explanations for each descriptor. These descriptors were primarily extracted computationally and refined through manual editing and redaction.

### Time signature

Time signatures are noted in the order of their first appearance in whole songs. For individual phrases, the relevant time signature for each phrase is labelled independently, regardless of the song's overall time signatures.

### Upbeat

This data specifies the number of quarter notes in an upbeat measure at the start of a tune (for example, 1.0 = 1 quarter note). It is primarily utilised for the Dezrann platform visualisation, but can also be valuable for music analyses.

### Number of music note units

This represents the total count of note units in a score, regardless of their duration. For example, both a quarter note and an eighth note are counted as 1 unit each.

### The number of measures

This descriptor indicates the number of all measures in a score representation of individual songs, including upbeats.

### Range

The entire song's range is expressed in two ways: first, as a melodic interval (e.g., m7 for minor 7th, M6 for major 6th, P8 for perfect 8th, etc.), and second, as an integer representing semitones (M6 for major 6th = 9, P8 for perfect 8th = 12, and so on).

### Pitch mean

The pitch mean of the entire song is expressed in an average MIDI value, with up to 2 decimals.

### Pitch direction

This descriptor indicates the relationship between the first and last notes of the song. Annotations include "+-" for descending, "-+" for ascending, and "equal" for the song starting and ending on the same pitch.

### Pitch variety or scale size

It quantifies the number of distinct pitch classes present in the score, disregarding octaves. It assigns a numerical value without specifying a particular scale name.

### Tone set

This information complements pitch variety by listing distinct pitch classes as letters without octaves, such as 5: E, C, A, B, and D, indicating the unique pitches present in the score.

### Leading tone

To enhance the specification of each song's tonal space, we indicate the presence or absence of a leading tone. In our context, where all tunes are transposed to G, the leading tone refers to F#. This detail further determines whether the song can be thought of as sung in major tonality or not.

### The number of phrases

This data provides a numerical annotation for the number of phrase units in each song. In the context of individual phrases, it labels each phrase with its ordinal number within the song, rather than the cumulative count of all phrases. Phrases were determined by combining the knowledge of no verse structure, punctuation marks in lyrics, and pauses in a tune.

### Phrase labels

These alphabetical annotations denote melodic deviations among phrases in each song. The initial phrase is designated as "A," and subsequent phrases are labelled alphabetically if their melody differs. If a phrase repeats the melodic material, it is assigned the letter of the original appearance. This descriptor was simplified from [Borsan 2023].

<img src="images/descriptors.png" width="600">

### Contours

These were computed for phrases only by the principle of David Huron’s melodic arches (Huron 1996). The computation involves analyzing the first and last MIDI values of the phrase and calculating the mean of all the values in between, provided there are more than 3 notes in the phrase. The annotations currently encompass all 9 Huron’s types of melodic contours or arches:

##### CONVEX (CVX)

<img src="images/CVX_247-6-1.png" width="600">

##### CONCAVE (CCV)

<img src="images/CCV_252-31-4.png" width="600">

##### ASCENDING (ASC)

<img src="images/ASC_237-5-1.png" width="600">

##### DESCENDING (DESC)

<img src="images/DESC_237-5-4.png" width="600">

##### ASCENDING HORIZONTAL (AH)

<img src="images/AH_252-61-1.png" width="600">

##### HORIZONTAL ASCENDING (HA)

<img src="images/HA_256-10-A3-3.png" width="600">

##### DESCENDING HORIZONTAL (DH)

<img src="images/DH_253-8-4.png" width="600">

##### HORIZONTAL DESCENDING (HD)

<img src="images/HD_248-19-1.png" width="600">

##### HORIZONTAL (H)

<img src="images/H_254-21-4.png" width="600">

### Melodic sequence

Melodic phrases were converted into alphanumeric sequences, representing note names and octaves, capturing detailed pitch information (for example, A4 = approx. 440 Hz). In some of the analyses, the octave information was omitted, and for the rest, the sequences were further converted into enumerated semitone interval successions. This conversion simplifies the representation of the musical data, focusing solely on the pitch relationships between notes without considering their specific octave positions.

### Verse: metric structure

These are numerical annotations, labelling the metric structure of the verse or several syllables in each phrase (see upper bottom labels in the example above). We acknowledge two types of syllabic annotations, the 'general', which describes the general idea of all verses, and 'specific', which annotates the specific phrase of the published first verse. Both are accessible in the annotation Excel tables (`slp_phrases.xlsx` and `slp_songs.xlsx`).

### Verse: rhyme structure

We took the rhyme structure annotations from the large collection of Slovenian folk song ballads. The annotations follow the alphabetical order, starting with the letter `M`, except `R`, which refers to refrain and `r`, which refers to a partial refrain within a larger structure. For this corpus, the two largest types (252, 286) were re-evaluated by GNI. In Dezrann, we display both, where the new differs from the original.