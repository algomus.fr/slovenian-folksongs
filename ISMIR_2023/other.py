import bitarray
import operator


def convert_to_excel(match_list, df):
    """This function converts the retrieved match list to excel file
    and saves it to local folder.

    Input: match_list (of any function), and dataframe
    Output: filtered dataframe, and excel file

    """
    idxs = []
    for x in range(0, len(match_list)):
        tune_id = match_list[x][0].split(".")
        tune_type = tune_id[0]
        tune_var = tune_id[1]
        tune_add = tune_id[2]
        tune_ph = tune_id[3]
        for idx, row in df.iterrows():
            if (
                str(row["Type"]) == tune_type
                and str(row["Variant"]) == tune_var
                and str(row["Additional_mark"]) == tune_add
                and str(row["Phrase_nr"]) == tune_ph
            ):
                idxs.append(idx)
    df2 = df.iloc[idxs]
    df2.to_excel("results.xlsx")
    return "Done. Check your folder."


def create_descriptor_bitarray(FEATURES):
    """
    This function creates a bitarray for selected features.
    Input: FEATURES (list, specifying the types of features)
    Output: a single bitarray (of those features)
    """
    descriptors = bitarray.bitarray("0000000000000000000000000000000")
    for key, value in FEATURES.items():
        print("{} allows the following keys {}.".format(key, value.keys()))
        x = input("Select {}".format(key))
        current_bits = FEATURES[key][x]
        descriptors = operator.or_(descriptors, current_bits)
    return descriptors
