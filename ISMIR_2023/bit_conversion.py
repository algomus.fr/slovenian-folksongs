import traceback
import bitarray
import pysdsl
import music21
import match_functions
import FEATURES_FILE as ff


def prepare_string(string):
    """Removes spaces and numbers,
    sharp and flat symbols from melodic string.
    It also adds $ sign at the end of the string.
    ---
    Example:
    string=A5B5A5
    new_string=prepare_string(string)
    new_string output:ABA"""
    phrase = string.replace(" ", "")
    no_space = ""
    digits = "0123456789#b"
    for ele in phrase:
        if ele in digits:
            phrase = phrase.replace(ele, no_space)
    phrase = phrase + "$"
    return phrase


def encode_string(full_phrase_string):
    """
    This function encodes string to suffixarray and bitarray,
    recognizing a "$" sign as devider between strings.
    Input: melodic sequence, preprocessed with prepare_string (type:str).
    Output: suffixarray, bitarray
    ---
    Example:
    string=ABABA
    new_string=encode_string(string)
    new_string output: ABABA$"""
    s_a = pysdsl.SuffixArrayBitcompressed(full_phrase_string)
    bit_vector = [0 if ele != "$" else 1 for ele in full_phrase_string]
    b_v = pysdsl.BitVector(bit_vector)
    return s_a, b_v


def convert_to_bits(
    dataframe, features, bit_list_filename="bit_list.txt", sa_bv_filenames=None
):
    """This function converts excel entry to:
    TUNE ID (type, variant, additional label, phrase number),
    BITARRAY* (all descriptors),
    BAR information (starting position of phrase), and
    INDEX NUMBER (used later to match melodies).
    It also returns a full melodic
    SUFFIXARRAY* (all melodic sequences combined,
        without octaves or any other symbols, e.g., A#5 = A).
    & BITARRAY of that suffix array.
    Input 1: Dataframe of tune descriptors*,
            type: pandas.dataframe
    Input 2: Pre-defined features in the form of bit arrays.*,
            type: dict of dicts(bitarrays)
    Input 3: file name to store results to txt file.
            type: str
            ** If title is left empty, the function will not save anything.
    Output : pb with types in the docstring: "return bit_list, s_a, b_v"
    *Built according to standards of this exact methodology!
    Available descriptors (all converted to string format):
    - Type
    - Variant
    - Phrase position
    - Phrase number
    - Phrase label
    - Phrase symbol
    - Phrase contour - general (ascending, descending, horizontal)
    - Phrase contour - specific (Huron's 9 contour types)
    - Phrase harmony (begining of phrase)
    - Phrase harmony (ending of phrase)
    - Toneset (number in string format)
    - Phrase meter(s)
    """
    bit_list = []
    full_phrase_string = ""
    number = 0
    if bit_list_filename != "":
        file = open(bit_list_filename, "w", encoding="utf-8")
    for idx, row in dataframe.iterrows():
        # PHRASE MELODY SEQUENCE
        phrase = str(row["Phrases"])
        # INDEX
        typ = str(row["Type"])
        var = str(row["Variant"])
        ph_nr = str(row["Phrase_nr"])
        add = str(row["Additional_mark"])
        ph_code = f"{typ}.{var}.{add}.{ph_nr}"
        # OTHER
        ph_pos = row["Phrase_position"]
        ph_label = row["Label"]
        ph_symbol = row["Label_Symbol"]
        ph_contour = row["Contours"]
        ph_start = row["PH_Start"]
        ph_end = row["PH_End"]
        ph_ct_gen = str(row["Contours_General"])
        ph_ts = row["Meter_s"]
        ph_ts = music21.meter.TimeSignature(ph_ts)
        ph_ts = ph_ts.classification  ## DESCRIBE THIS, EXCEPTION
        ph_scale = int(row["Pitch_variety"])
        measure = str(row["Measure_Phrase"])
        generic = bitarray.bitarray(ff.GENERIC)
        try:
            descriptors_bit = bitarray.bitarray(generic)
            # Phrase position
            descriptors_bit |= features[ff.PHRASE_POS][ph_pos]
            # Phrase number
            descriptors_bit |= features[ff.PHRASE_NR][ph_nr]
            # Phrase label
            descriptors_bit |= features[ff.PHRASE_LBL][ph_label]
            # Label symbol
            descriptors_bit |= features[ff.PHRASE_SYM][ph_symbol]
            # Contour General - already in bits, has to be re-edited
            descriptors_bit |= features[ff.PHRASE_CTGEN][ph_ct_gen]
            # Contour Specific
            descriptors_bit |= features[ff.PHRASE_CTSPEC][ph_contour]
            # Starting harmony
            descriptors_bit |= features[ff.PHRASE_HS][ph_start]
            # Ending harmony
            descriptors_bit |= features[ff.PHRASE_HE][ph_end]
            # Time signature
            descriptors_bit |= features[ff.PHRASE_TS][ph_ts]
            # Phrase scale
            descriptors_bit |= features[ff.PHRASE_SCALE][str(ph_scale)]
            ##
            entry = [ph_code, descriptors_bit, measure, number]
            number = number + 1
            # APPEND THE DUO TO COMMON LIST
            bit_list.append(entry)
            ##Removes unnecessary symbols from string
            phrase_string = prepare_string(phrase)
            ##Adds positions of each phrase in full string to phrase id.
            full_phrase_string = full_phrase_string + phrase_string
        except:
            traceback.print_exc()
    # get suffixarray and bitarray of combined melody string
    s_a, b_v = encode_string(full_phrase_string)

    if sa_bv_filenames is not None:
        match_functions.save_sa_bv(s_a, b_v, sa_bv_filenames[0], sa_bv_filenames[1])

    # successful cases displayed (in number)
    successful_cases = len(bit_list)
    print("Successfully converted {} sequences".format(successful_cases))
    if bit_list_filename != "":
        for i in bit_list:
            file.write(str(i) + "\n")
        file.close()
    return bit_list, s_a, b_v
