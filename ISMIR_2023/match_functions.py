import bitarray
from bitarray import util
import pysdsl


######### LOAD SA and BV ########
def load_sa_bv(s_a_filename, b_v_filename):
    s_a = pysdsl.SuffixArrayBitcompressed.load_from_file(s_a_filename)
    b_v = pysdsl.BitVector.load_from_file(b_v_filename)
    return s_a, b_v


######### SAVE SA and BV #########
def save_sa_bv(s_a, b_v, s_a_filename, b_v_filename):
    return s_a.store_to_file(s_a_filename) and b_v.store_to_file(b_v_filename)


######### MELODY ###########
def melody_match(melody, bit_db, s_a, b_v):
    """
    This function matches the desired melodic sequence query with
    pre-converted database (done with bit_conversion.convert_to_bits).

    Input: melody (type: str)
    Input: bit_db (type: list of lists)
    Input: s_a (type: suffix array)
    Input: b_v (type: bit vector)

    Output: Melodic matches with position in score. (type: list of lists)

    ---
    Example:

    melody=ABA
    bit_db, s_a, b_v=bit_conversion.convert_to_bits(df)
    matches=melody_match(melody, bit_db, s_a, b_v)

    matches output: "Your query was found in XX at position YY"
    """
    matches = []
    match_pos = s_a.locate(melody)
    rnk = b_v.init_rank_1()
    slct = b_v.init_select_1()
    for pos in match_pos:
        phrase_idx = rnk.rank(pos)
        b_id = bit_db[phrase_idx][0]
        ph_bar = bit_db[phrase_idx][2]
        ph_nr = bit_db[phrase_idx][3]
        if phrase_idx == 0:
            melody_pos = pos
            matches.append([b_id, ph_nr, ph_bar, melody_pos])
        else:
            melody_pos = pos - slct.select(phrase_idx)
            matches.append([b_id, ph_nr, ph_bar, melody_pos])

    return matches


#### BIT MASK ###
def compare_bits(a_query, b_query, mask):
    """
    This function compares the two bits with
    costum made mask, with xor operator.
    """
    xor = a_query ^ b_query
    result = mask & xor
    final_result_mask = util.ba2int(result)
    return final_result_mask


######### MATCH WITH MASK ###########
def descriptor_match(query, bit_db, mask, melody_matches):
    """
    "descriptor_match" function allows the user to match their query with a_query desired database.
    It also allows them to apply mask, if they wish. It can be applied either to descriptors only,
    or within find_all function, where melody is also matched. If the first one is applied,
    melody matches should be set to None.

    Input: query_tuple (type: tuple of strings)
    Input: bit_db (type: list of tuples (str, bitarray), retrieved with "convert_to_bits")
    Input: mask (type: bitarray, can be retrieved with "create_mask")
    Input: pos (type: int) - optional, can be set to "None"
    Output: Every match prints out id of both, query and tune in database along the way.
    Output: List of matched tune ids (type: str), excluding initial query (if query is in database).

    ----
    Example:
    query=(tune_id_or_random, '0101110000000110010001110111101')
    bit_db, s_a, b_v=bit_conversion.convert_to_bits(df)
    mask=mask.create_mask(DESCRIPTOR1, DESCRIPTOR2)
    *Optional:
    *melody=ABA
    *melody_matches=melody_match(melody, bit_db, s_a, b_v)
    matches=descriptor_match(query, bit_db, mask, melody_matches)
    """
    a_query = bitarray.bitarray(query[1])
    match_list = []

    if melody_matches is None:
        for tune in bit_db:
            b_bar = tune[2]
            b_query = tune[1]
            b_id = tune[0]
            b_pos = None
            # q_type="DESCRIPTORS"
            final_result_mask = compare_bits(a_query, b_query, mask)
            if final_result_mask == 0:
                match_list.append([b_id, b_query, b_bar])

    else:
        for match in melody_matches:
            b_id = bit_db[(match[1])][0]
            b_query = bit_db[(match[1])][1]
            b_bar = bit_db[(match[1])][2]
            b_pos = match[3]
            #  q_type="MELODY+DESCRIPTORS"
            final_result_mask = compare_bits(a_query, b_query, mask)
            if final_result_mask == 0:
                match_list.append([b_id, b_query, b_bar, b_pos])
    return match_list


######### FIND ALL ###########


def find_all(query, melody, mask, bit_db, s_a, b_v):
    """
    This function combines the melody and descriptor search.
    Input: query (type:tuple)
    Input: melody (type:str)
    Input: mask (type:bitarray)
    Input: bit_db (type:list of lists)
    Input: s_a (type:suffix array)
    Input: b_v (type:bitarray)
    Output: all_matches (type: list of lists - matches matched both, by melody and descriptors)

    ---
    Example:
    query=(tune_id_or_random, '0101110000000110010001110111101')
    bit_db, s_a, b_v=bit_conversion.convert_to_bits(df)
    mask=mask.create_mask(DESCRIPTOR1, DESCRIPTOR2)
    melody=ABA
    matches=find_all(query, melody, mask, bit_db, s_a, b_v)

    """
    melody_matches = melody_match(melody, bit_db, s_a, b_v)
    if "0" in mask.to01():
        mask_applied = "MASK APPLIED"
    else:
        mask_applied = "MASK NOT APPLIED"
    match_list = descriptor_match(query, bit_db, mask, melody_matches)
    return match_list
