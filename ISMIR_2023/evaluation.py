import pandas as pd
import bitarray
import match_functions
import FEATURES_FILE as ff


def evaluate(
    queries, slp_index, subset_index, subset_index_f, subset_index_m, subset_index_l
):
    lst = []
    for i in range(0, len(queries)):
        descriptors_bit = bitarray.bitarray(ff.GENERIC)
        for item in queries[i]["Descriptors"]:
            descriptors_bit = descriptors_bit | item
        query = (queries[i]["Comment"], descriptors_bit)
        melody = queries[i]["Melody"]
        mask = queries[i]["Mask"]
        phrase = queries[i]["Phrase"]
        md_slp = match_functions.find_all(
            query, melody, mask, slp_index["DB"], slp_index["SA"], slp_index["BV"]
        )
        md_subset = match_functions.find_all(
            query,
            melody,
            mask,
            subset_index["DB"],
            subset_index["SA"],
            subset_index["BV"],
        )
        md_st_f = match_functions.find_all(
            query,
            melody,
            mask,
            subset_index_f["DB"],
            subset_index_f["SA"],
            subset_index_f["BV"],
        )
        md_st_m = match_functions.find_all(
            query,
            melody,
            mask,
            subset_index_m["DB"],
            subset_index_m["SA"],
            subset_index_m["BV"],
        )
        md_st_l = match_functions.find_all(
            query,
            melody,
            mask,
            subset_index_l["DB"],
            subset_index_l["SA"],
            subset_index_l["BV"],
        )
        ##GENERAL SLP
        am_slp = []
        for i in range(0, len(md_slp)):
            am_slp.append(md_slp[i][0])
        unique_slp = len(set(am_slp))
        ##FIRST PHRASES ONLY###
        if phrase == "F":
            am_st_f = []
            for i in range(0, len(md_st_f)):
                am_st_f.append(md_st_f[i][0])
            unique_st_f = len(set(am_st_f))
            all_st = len(subset_index_f["DB"])
            t_p = unique_st_f
            f_p = unique_slp - t_p
            f_n = all_st - t_p
            precision = t_p / (t_p + f_p)
            recall = t_p / (t_p + f_n)
            try:
                f1_score = 2 * (precision * recall / (precision + recall))
            except:
                f1_score = "ERROR"
        ###MIDDLE PHRASES ONLY###
        elif phrase == "M(3)":
            ###
            am_st_m = []
            for i in range(0, len(md_st_m)):
                am_st_m.append(md_st_m[i][0])
            unique_st_m = len(set(am_st_m))
            ###
            all_st = len(subset_index_m["DB"])
            t_p = unique_st_m
            f_p = unique_slp - t_p
            f_n = all_st - t_p
            precision = t_p / (t_p + f_p)
            recall = t_p / (t_p + f_n)
            try:
                f1_score = 2 * (precision * recall / (precision + recall))
            except:
                f1_score = "ERROR"
        ###LAST PHRASES ONLY###
        elif phrase == "L":
            ###
            am_st_l = []
            for i in range(0, len(md_st_l)):
                am_st_l.append(md_st_l[i][0])
            unique_st_l = len(set(am_st_l))
            ###
            all_st = len(subset_index_l["DB"])
            t_p = unique_st_l
            f_p = unique_slp - t_p
            f_n = all_st - t_p
            precision = t_p / (t_p + f_p)
            recall = t_p / (t_p + f_n)
            try:
                f1_score = 2 * (precision * recall / (precision + recall))
            except:
                f1_score = "ERROR"
        else:
            am_subset = []
            for i in range(0, len(md_subset)):
                am_subset.append(md_subset[i][0])
            unique_subset = len(set(am_subset))
            all_st = len(subset_index["DB"])
            t_p = unique_subset
            f_p = unique_slp - t_p
            f_n = all_st - t_p
            precision = t_p / (t_p + f_p)
            recall = t_p / (t_p + f_n)
            try:
                f1_score = 2 * (precision * recall / (precision + recall))
            except:
                f1_score = 0
        lst.append(
            [
                query[0],
                phrase,
                t_p,
                f_p,
                f_n,
                round(precision, 2),
                round(recall, 2),
                round(f1_score, 2),
            ]
        )
    dataframe = pd.DataFrame(
        lst, columns=["Q", "Phrase", "TP", "FP", "FN", "Precision", "Recall", "F1"]
    )
    return dataframe
