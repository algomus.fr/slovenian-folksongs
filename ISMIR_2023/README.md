# Adding context to content improves pattern matching: A study on Slovenian folksongs


Pattern matching is a widely spread topic in MIR and related fields. It commonly aims to provide insights into repetitive instances in or across different kinds of music or cultures. The paper presents a study on the analysis of folksongs using symbolic music representation, including both music content and its contextual information. 


## Reference

Vanessa Nina Borsan, Mathieu Giraud, Richard Groult, Thierry Lecroq. [Adding context to content improves pattern matching: A study on Slovenian folksongs](https://hal.science/hal-04142315v1/file/2023-ismir-descriptors-pattern-matching.pdf), International Society for Music Information Retrieval Conference (ISMIR 2023), 2023, pp 474-481. 

## Released data and code

### Dataset

- `tunes.xlsx`: 400 tunes, with descriptors for each tune
- `phrases.xlss`: 1502 phrases, with descriptors for each phrase

These data are made available under the [Open Database License](http://opendatacommons.org/licenses/odbl/1.0/).
Any rights in individual contents of the database are licensed under the [Database Contents License](http://opendatacommons.org/licenses/dbcl/1.0/).


### Code

- `bit_conversion.py`: converts annotations to bit vectors and suffix array.
- `match_functions.py`: match, selected descriptors and/or melody in database with input queries.
- `mask.py`: ignores all bit positions in descriptor (or full) matching, which correspond with undesired descriptor positions (see BIT Representation below).
- `evaluation.py`: evaluates algorithm performance based on manually curated subset of melodically similar tunes. 
- `FEATURES_FILE.py`: all current descriptors are defined in this script.
- `other.py`: pd to excel and other conversions.

### List of descriptors

#### ID
- Type (1,2)
*Each tune in the collection has a pre-assigned identifier. Type was assigned according to thematic content of tune (e.g., lyrics)*
- Variant (1,2)
*Each type can have several variants, ranging from 1 to over 200. We only release the variants, which correspond to our music requirements (monophonic symbolic transcription available).*
- Additional_mark (1,2)
*Some types and/or variants are additionally sub-categorized with letters (A, B, C, etc.)*

#### LYRIC
- First_line (1,2)
*First line or lyric of the tune (by ID) in original language.*
- First_line_translation (1,2)
*First line or lyric of the tune (by ID) in english translation.*

#### METADATA
- Region (1,2)
*Origin of the transcription*
- Annotator (1,2)
*Annotator's initials (First name, Last name)*
- Year_of_annotation (1,2)
*Year when the tune was collected.*
- Singers_enumerated (1,2)
*Singer information - gender and group. 0=; 1=;     *

#### MUSICAL CONTEXT - TUNE
- Voices (1,2)
*Number of voices annotated in tune.*
- Notes_Nr (1,2)
*Number notes in tune.*
- Measure_Nr (1,2)
*Number of measures/bars in tune.*
- Ambitus_interval (1,2)
*Voice range in intervals of tune.*
- Ambitus_semitones (1,2)
*Voice range in semitones of tune.*
- Pitch_mean (1,2)
*Pitch mean of tune.*
- Pitch_direction (1,2)
*General direction of pitch: +- (descending), -+ (ascending), equal.*
- Pitch_variety (1,2)
*Number pitch classes in tune*
- Tone_set (1,2)
*All pitch classes in tune*
- HAS_F# (1,2)
*Has leading tone or not? (all tunes are transcribed to G!)*
- Phrase_labels (1)
*Consecutive phrase labels in a sequence, without label symbols (see below).*

#### MUSICAL CONTEXT - PHRASE
- Meter_s (2)
*All time signatures of phrase. There were numerous rhythmic combinations, after being retrieved with music21. Hence, in later steps, we used music21 function (TimeSignature), which textually describes groups of time signatures (e.g., “simple triple”, instead of 3/4).*
- Phrase_nr (1 (number of phrases in tune),2 (numbered phrase))
*A number of all phrases in tune OR a numbered phrase in tune.*
- Phrase_position (2)
*Position of phrase in tune: First, Middle, Last.*
- Phrases (2)
*Melodic sequence of a phrase.*
- Label (2)
*Label of a phrase (assigned based on repetition of melodic material within tune).*
- Label_Symbol (2)
*If label above varied but closely resambled previous phrase (I, ' or II, '', or III, '''), if its melodic material almost identified as new (P, +), or the phrase was repeated but this time played a role of chorus/refrain (X).
- Contours_General (2)
*Ascending, descending, or relatively horizontal tune contour.*
- Contours (2)
*Huron's 9 contour types: Convex (-+-), Concave (+-+), Ascending (-+), Descending (+-), Horizontal Descending (=-), Descending Horizontal (-=), Ascending Horizontal (+=), Horizontal Ascending (=+), Horizontal (===)*
- Measure_Phrase (2)
*Starting position of phrase within specific measure.*
- PH_Start (2)
*Starting harmonic frame of phrase.*
- PH_End (2)
*Ending harmonic frame of phrase.*

### Bit representation of selected descriptors
Below, there are the currently pre-set descriptors, that can be represented and matched as a bitarray. 
Bit array consists of 32 bits, separated into sections (position in array is indicated in brackets '[]'):
`XX-XXX-XXX-XXXX-XX-XXXX-XXX-XXX-XXXX-XXX`
- EXAMPLE:
* 10-000-111-0000-01-1111-111-111-0110-101
* * F- 1-  A-  N-  (+)- ASC- T  -T -SimpleDuple -6
- FML [0:2]
    * F: 10
    * M: 11
    * L: 01
- NUMBER [2:5]
    * 1: 000
    * 2: 001
    * 3: 011
    * 4: 111
    * 5: 100
    * 6: 110
    * 7: 101
    * 8: 010
- LABEL	[5:8]
    * A: 111
    * B: 000
    * C: 100
    * D: 001
    * X: 101
- SYMBOL [8:12]
    * N: 0000
    * I: 0001
    * II: 0011
    * III: 0111
    * P: 1010
    * PI: 1011
    * X: 1001
    * XI: 1101
    * PX: 1111
    * IX: 0110
    * IX: 0110
- CONTOUR_GENERAL [12:14]
    * Generally ascending: 01
    * Generally descending: 10
    * Horizontal: 11
    * Other: 00
- CONTOUR [14:18]	
    * CVX: 1001
    * CCV: 0110
    * ASC: 1111 
    * HA: 1000
    * AH: 1100
    * DESC: 0000
    * HD: 0111
    * DH: 0011
    * H: 1010
- HARMONY_START [18:21]	
    * ?: 101
    * ?(D/T): 010
    * ?(D): 011
    * ?(T): 100
    * D/T: 110
    * D: 000
    * T: 111
- HARMONY_END [21:24]
*Same as HARMONY_START*
- TIME_SIGNATURE (music21 method) [24:28]	
    * Compound Duple: 1100
    * Compound Triple: 1110
    * Other Duple: 1001
    * Other Single: 1000
    * Simple Duple: 0110
    * Simple Quadruple: 1111
    * Simple Quintuple: 1011
    * Simple Septuple: 1010
    * Simple Triple: 0111
- SCALE	[28:31]
    * 3: 111
    * 4: 110
    * 5: 100
    * 6: 101
    * 7: 011
    * 8: 010
    * None: 000

### Phrases (melodic sequence)
- Phrase is represented as melodic pitch sequence in letters without octave information.
- All phrases are concatenated to one single string.
- To retrieve their position and other information, one can reconstruct them by using suffix array and bitvector representation.

