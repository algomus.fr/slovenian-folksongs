import bitarray
import FEATURES_FILE as ff


def create_mask(features):
    """
    This function creates a mask based on user's inputs.
    Input: set of features one wants to include (type: list)
    Output: mask (type: bitarray)
    !Available descriptors always correspond to those in function "convert_to_bits".
    List of current descriptors:
    - Phrase position
    - Phrase number
    - Phrase label
    - Phrase symbol
    - Phrase contour - general (ascending, descending, horizontal)
    - Phrase contour - specific (Huron's 9 contour types,
        source: http://www.music.mcgill.ca/~ich/classes/mumt621_20/papers/huron96melodic.pdf)
    - Phrase harmony (begining of phrase)
    - Phrase harmony (ending of phrase)
    - Toneset (number in a string format)
    - Phrase meter(s), simplified
        (source: music21.meter.TimeSignature.classification)
    Note 1: Mask can be simply altered by choosing a different set of features.
    Note 2: If you wish to exclude the mask from the process,
            choose none of the features. The mask will result in zeroes,
            which will, in fact, let the algorithm run all of them.
            The same works if one chooses all features.
    Note 3: If you wish, you can create the mask manually.
            However, beware to understand the structure of bitarrays
            created with "convert_to_bits." No features,
            that are not pre-defined in functions can be applied
            without code alteration!
    Note 3.1: To make your own array, create a string of 0s and 1s,
                and run bitarray('your_sequence').
                "bitarray" library is an external library
                and is available here: https://pypi.org/project/bitarray/.
    ----
    Example:
    my_mask=mask.create_mask([ff.PHRASE_POS, ff.PHRASE_SCALE])
    my_mask output: bitarray(1100000000000000000000000000111)
    """
    mask = bitarray.bitarray(ff.GENERIC)
    if len(features) != 0:
        for feature in features:
            mask |= ff.FEATURES[feature][ff.BIT_ARRAY]
    else:
        mask = bitarray.bitarray(ff.GENERIC_1s)
    return mask
