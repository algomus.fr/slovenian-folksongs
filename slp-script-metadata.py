import pandas as pd
import ast
import json


df = pd.read_excel("slp-data/slp_songs.xlsx")
data_dict = {}

def sort_pitch_classes(input_string):
    # Define the pitch classes in musical order
    musical_order = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#','Bb','B']
    try:
        # Remove spaces and split the input string into a list of pitch classes
        pitch_classes = [pc.strip() for pc in input_string.split(',')]

        # Find the starting pitch class (G or the next pitch class after G)
        start_index = musical_order.index('G') if 'G' in pitch_classes else (musical_order.index('G') + 1) % 12

        # Sort the pitch classes starting from the found index
        sorted_pitch_classes = sorted(pitch_classes, key=lambda x: ((musical_order.index(x) - start_index) % 12, x))

        # Join the sorted pitch classes into a comma-separated string
        output_string = ', '.join(sorted_pitch_classes)
    except:
        output_string="Unknown"

    return output_string

def slovenian_toneset(input_string):
    # Replace 'Bb' with a temporary placeholder
    input_string = input_string.replace('Bb', 'TEMP_PLACEHOLDER')

    # Replace 'B' with 'H'
    input_string = input_string.replace('B', 'H')

    # Replace the temporary placeholder with 'B'
    input_string = input_string.replace('TEMP_PLACEHOLDER', 'B')

    return input_string

# df iteration
for index, row in df.iterrows():
    # ID elements
    element_type = str(row["Type"])
    element_variant = str(row["Variant"])
    element_additional_mark = str(row["Additional_mark"])

    # ID: type, variant, and additional_mark (if available)
    id_components = [element_type, element_variant]
    if element_additional_mark != "nan":
        id_components.append("-"+element_additional_mark)
    id_tune = "-".join(id_components)

    # change form of toneset
    toneset_string = sort_pitch_classes(row["Tone_set"])

    # filter out time signature
    time_signature = str(row["Time_signatures"])
    time_signature_list = time_signature.split(", ")
    if len(time_signature_list) > 1:
        time_signature = time_signature_list[0]

    # dict row-key:value
    row_dict = {
        "piece:num": element_type,
        "piece:title": None if pd.isna(row["SLP_Type_title"]) else row["SLP_Type_title"], 
        "piece:title:en": None if pd.isna(row["SLP_Type_title_en"]) else row["SLP_Type_title_en"],
        
        "variant:num": element_variant,
        "variant:first": None if pd.isna(row["First_line"]) else row["First_line"],
        "variant:first:en": None if pd.isna(row["First_line_en"]) else row["First_line_en"],
        
        "meter:poetic": None if pd.isna(row["Lyric_structure_overall"]) else row["Lyric_structure_overall"],

        "origin:region": None if pd.isna(row["Region"]) else row["Region"],
        "origin:region:en": None if pd.isna(row["Region_en"]) else row["Region_en"],
        
        # Check and exclude elements with specified conditions
        "key:pcs": slovenian_toneset(toneset_string) if pd.notna(row["Tone_set"]) and str(row["Tone_set"]).lower() not in ["none", "nan", ""] else None,
        "key:pcs:en": toneset_string if pd.notna(row["Tone_set"]) and str(row["Tone_set"]).lower() not in ["none", "nan", ""] else None,
        "recording": None if pd.isna(row["rec"]) else row["rec"],
        
        "year": None if pd.isna(row["Year_of_transcription_recording"]) else row["Year_of_transcription_recording"], 
        
        ## METADATA - contributors
        "contributors": {
            "collector": None if pd.isna(row["Transcriber_collector"]) else row["Transcriber_collector"],
            "singer": None if pd.isna(row["Singers_name"]) else row["Singers_name"],
            "singer:type": None if pd.isna(row["Singers_type"]) else row["Singers_type"],
        },
    }

    # Remove key-value pairs with None values from the dictionary
    row_dict = {k: v for k, v in row_dict.items() if v is not None and not pd.isna(v)}

    # ID
    data_dict[id_tune] = {'opus': row_dict}

# metadata dict
metadata = {
    "template": { "opus" : {
        'collection:title': 'Slovenske ljudske pripovedne pesmi', 
        'collection:title:en': 'Slovenian Folk Ballads', 
        'collection:topic:en': 'Družinske pripovedne pesmi', 
        'collection:topic': 'Family ballads', 
        "id": "{KEY}",
        "opus": "slp-{KEY}",
        "contributors": {
            "editor": "Vanessa Nina Borsan, GNI ZRC SAZU",
            "annotators": "Vanessa Nina Borsan, GNI ZRC SAZU",
            "encoder": "GNI ZRC SAZU, Matevž Pesek, Matija Marolt, Vanessa Nina Borsan",
        },
        "year": "{year}",
        "topic": "družinske pesmi",
        "topic:en": "family ballad",
        "genre": "pripovedne pesmi",
        "genre:en":"ballad",
        "key": "G (major mode)",
        "measure-map": "slp-scores/{tunetype}-{var}.mm.json",

        "..quality:audio": "4",
        "..quality:audio:synchro": "4",

        "..sources.0.score": "slp-scores/{tunetype}-{var}.mxl",

        "..sources.1.analysis": "slp-annotations/{tunetype}-{var}.dez",
        "..analysis:default": "{tunetype}-{var}.dez",

        "..sources.2.audio": "slp-audio/{tunetype}-{var}.mp3",
        "..sources.2.synchro": "slp-synchro/slp-{tunetype}-{var}-synchro.json",

        "ref:publication":"XXX"
    }},
    "pieces": data_dict
}

# Remove key-value pairs with None values from the metadata dictionary
metadata = {k: v for k, v in metadata.items() if v is not None and not pd.isna(v)}

# Write the metadata dictionary to a JSON file
with open("slp-metadata/slp-metadata.json", "w") as file:
    json.dump(metadata, file, indent=2, sort_keys=True)
    
